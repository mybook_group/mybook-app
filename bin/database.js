// database.js
import mysql from 'mysql2/promise';
import 'dotenv/config';

const { PORT, DB_NAME, DB_HOST, DB_USER, DB_PASSWORD, API_KEY } = process.env;

const pool = mysql.createPool({
	port: PORT,
	host: DB_HOST,
	user: DB_USER,
	password: DB_PASSWORD,
	database: DB_NAME,
	apiKey: API_KEY,
	waitForConnections: true,
	connectionLimit: 10,
	queueLimit: 0,
});

export default pool;
