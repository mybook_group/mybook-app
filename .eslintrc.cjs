module.exports = {
	env: {
		node: true,
		es2021: true,
	},
	extends: ['eslint:recommended'],
	plugins: ['prettier'],
	overrides: [
		{
			env: {
				node: true,
			},
			files: ['.eslintrc.{js,cjs}'],
			parserOptions: {
				sourceType: 'script',
			},
		},
	],
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module',
	},
	rules: {
		'prettier/prettier': 'error',
		'prefer-const': 'error',
		semi: ['error', 'always'],
		// 'indent': [
		// 	'off',
		// 	'tab'
		// ],
		'linebreak-style': ['warn', 'unix'],
		quotes: ['off', 'single'],
		'no-mixed-spaces-and-tabs': 'off',
		'no-unused-vars': 'warn',
	},
};
