# mybook-app

## Development

### How to start development environment

1. Clone this repository and enter this repo

2. install with npm

```bash
$ npm ci
```

3. start app

```bash
$ npm run dev
```
