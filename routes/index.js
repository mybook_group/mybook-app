import express from 'express';
var router = express.Router();
import db from '../database.js';
/* GET home page. */
router.get('/', function (req, res) {
	res.render('index', { title: 'Express' });
});

/*connect to mysql */
router.get('/', async (req, res) => {
	try {
		const [rows] = await db.query('SELECT * FROM users');
		res.json(rows);
	} catch (err) {
		console.error(err);
		res.status(500).send('Server Error');
	}
});

export default router;

module.exports = router;
