import express from 'express';
var router = express.Router();
import db from '../database.js';
/* GET users listing. */
router.get('/', function (req, res) {
	res.send('respond with a resource');
});

/*connect to mysql */
router.get('/', async (req, res) => {
	try {
		const [rows] = await db.query('SELECT * FROM users');
		res.json(rows);
	} catch (err) {
		console.error(err);
		res.status(500).send('Server Error');
	}
});

module.exports = router;
